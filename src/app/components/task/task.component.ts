import { Component, Input, OnInit } from '@angular/core';
import { TaskI } from 'src/app/models/task';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  @Input('task') task!: TaskI;
  // tasks!: TaskI[];

  constructor(public dataService: DataService) { }

  ngOnInit(): void {
    // this.tasks = this.dataService.getTask();
  }
  
  removeTask(task: TaskI){
    const response = confirm('are you sure to delete it?')
    if(response){
      this.dataService.removeTask(task)
    }
  }

}
