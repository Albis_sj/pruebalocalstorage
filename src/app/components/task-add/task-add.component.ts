import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { TaskI } from 'src/app/models/task';

@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.css']
})
export class TaskAddComponent implements OnInit {
  title!: string;
  description!: string;
  @Output() taskAdded = new EventEmitter<TaskI>();

  constructor() { }

  ngOnInit(): void {
  }

  addTask(): void {
    console.log('envio del submit');
    console.log(this.title, 'tit');
    console.log(this.description, 'des');
    
    this.taskAdded.emit({
      title: this.title,
      descripcion: this.description,
      hide: true 
    })
    this.title = '';
    this.description = '';
  }

}
